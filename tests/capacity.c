// SPDX-License-Identifier: LGPL-2.1-or-later
#include "util.h"

enum {
	TEST_FILE_SIZE = 2 * 1024 * 1024,
};

static char filename[] = "capacity-XXXXXX";

static void cleanup(void)
{
	unlink(filename);
}

int main(void)
{
	struct blkio *b;
	char *errmsg;
	uint64_t capacity;
	int fd;

	ok(blkio_create("io_uring", &b, &errmsg));
	assert(b);

	err(blkio_get_uint64(b, "capacity", &capacity, &errmsg), -ENODEV);
	err(blkio_set_uint64(b, "capacity", 4096, &errmsg), -EACCES);

	register_cleanup(cleanup);
	fd = create_file(filename, TEST_FILE_SIZE);

	ok(blkio_set_int(b, "fd", fd, &errmsg));
	ok(blkio_set_bool(b, "initialized", true, &errmsg));

	fd = -1; /* ownership passed to libblkio */

	ok(blkio_get_uint64(b, "capacity", &capacity, &errmsg));
	assert(capacity == TEST_FILE_SIZE);

	err(blkio_set_uint64(b, "capacity", 4096, &errmsg), -EACCES);

	ok(blkio_get_uint64(b, "capacity", &capacity, &errmsg));
	assert(capacity == TEST_FILE_SIZE);

	blkio_destroy(&b);
	assert(!b);

	return 0;
}
