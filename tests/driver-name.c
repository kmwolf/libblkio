// SPDX-License-Identifier: LGPL-2.1-or-later
#include <string.h>
#include "util.h"

int main(void)
{
	struct blkio *b;
	char *errmsg;
	char *driver = NULL;

	ok(blkio_create("io_uring", &b, &errmsg));
	assert(b);

	ok(blkio_get_str(b, "driver", &driver, &errmsg));
	assert(driver);
	assert(strcmp(driver, "io_uring") == 0);
	free(driver);
	driver = NULL;

	err(blkio_set_str(b, "driver", "foo", &errmsg), -EACCES);

	/* Check driver has not changed */
	ok(blkio_get_str(b, "driver", &driver, &errmsg));
	assert(driver);
	assert(strcmp(driver, "io_uring") == 0);
	free(driver);

	blkio_destroy(&b);
	assert(!b);

	return 0;
}

