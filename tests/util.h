// SPDX-License-Identifier: LGPL-2.1-or-later
/* Testing utility functions */

#ifndef TESTS_UTIL_H
#define TESTS_UTIL_H

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <unistd.h>
#include "blkio.h"

/* Check that expr is 0 and errmsg was not set */
#define ok(expr) \
	do { \
		int ret = (expr); \
		if (ret != 0) { \
			fprintf(stderr, "%s failed (ret %d): %s\n", #expr, ret, errmsg); \
			abort(); \
		} \
		assert(!errmsg); \
	} while (0)

/* Check that expr equals expected and errmsg was set. Frees errmsg. */
#define err(expr, expected) \
	do { \
		int ret = (expr); \
		if (ret != (expected)) { \
			fprintf(stderr, "%s expected return value %d, got %d\n", \
			        #expr, (expected), ret); \
			abort(); \
		} \
		assert(errmsg); \
		free(errmsg); \
		errmsg = NULL; \
	} while (0)

void register_cleanup(void (*fn)(void));
int create_file(char *namebuf, off_t length);

#endif /* TESTS_UTIL_H */
