// SPDX-License-Identifier: LGPL-2.1-or-later
#include <sys/mman.h>
#include "util.h"

enum {
    TEST_FILE_SIZE = 2 * 1024 * 1024,
    TEST_FILE_OFFSET = 0x10000,
};

static char filename[] = "read-XXXXXX";

static void cleanup(void)
{
    unlink(filename);
}

/*
 * Read a well-known pattern from an offset in a file.
 */
int main(void)
{
    struct blkio *b;
    struct blkioq *q;
    struct blkio_mem_region mem_region;
    struct blkio_completion completion;
    void *buf;     /* I/O buffer */
    void *pattern; /* reference data at TEST_FILE_OFFSET */
    size_t buf_size;
    char *errmsg;
    int fd;
    int mfd;

    /* Set up I/O buffer */
    buf_size = sysconf(_SC_PAGESIZE);
    mfd = memfd_create("buf", MFD_CLOEXEC);
    assert(mfd >= 0);
    assert(ftruncate(mfd, buf_size) == 0);
    buf = mmap(NULL, buf_size, PROT_READ | PROT_WRITE, MAP_SHARED, mfd, 0);
    assert(buf != MAP_FAILED);

    mem_region = (struct blkio_mem_region) {
        .addr = buf,
        .len = buf_size,
        .fd_offset = 0,
        .fd = mfd,
    };

    /* Initialize pattern buffer */
    pattern = malloc(buf_size);
    assert(pattern);
    memset(pattern, 'A', buf_size);

    ok(blkio_create("io_uring", &b, &errmsg));
    assert(b);

    register_cleanup(cleanup);
    fd = create_file(filename, TEST_FILE_SIZE);
    assert(pwrite(fd, pattern, buf_size, TEST_FILE_OFFSET) == buf_size);

    ok(blkio_set_int(b, "fd", fd, &errmsg));
    ok(blkio_set_bool(b, "initialized", true, &errmsg));

    fd = -1; /* ownership passed to libblkio */

    ok(blkio_set_int(b, "num-queues", 1, &errmsg));
    ok(blkio_set_bool(b, "started", true, &errmsg));
    ok(blkio_add_mem_region(b, &mem_region, &errmsg));

    q = blkio_get_queue(b, 0);
    assert(q);

    assert(blkioq_read(q, TEST_FILE_OFFSET, buf, buf_size, NULL, 0) == 0);
    assert(blkioq_submit_and_wait(q, 1, NULL, NULL) == 0);
    assert(blkioq_get_completions(q, &completion, 1) == 1);
    assert(completion.ret == buf_size);
    assert(memcmp(buf, pattern, buf_size) == 0);

    blkio_destroy(&b);
    return 0;
}
