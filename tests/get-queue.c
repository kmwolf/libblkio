// SPDX-License-Identifier: LGPL-2.1-or-later
#include "util.h"

enum {
	TEST_FILE_SIZE = 2 * 1024 * 1024,
};

static char filename[] = "started-XXXXXX";

static void cleanup(void)
{
	unlink(filename);
}

int main(void)
{
	struct blkio *b;
	char *errmsg;
	int fd;

	ok(blkio_create("io_uring", &b, &errmsg));
	assert(b);

	assert(blkio_get_queue(b, -1) == NULL);
	assert(blkio_get_queue(b, 0) == NULL);
	assert(blkio_get_queue(b, 1) == NULL);

	register_cleanup(cleanup);
	fd = create_file(filename, TEST_FILE_SIZE);

	ok(blkio_set_int(b, "fd", fd, &errmsg));
	ok(blkio_set_bool(b, "initialized", true, &errmsg));

	fd = -1; /* ownership passed to libblkio */

	assert(blkio_get_queue(b, -1) == NULL);
	assert(blkio_get_queue(b, 0) == NULL);
	assert(blkio_get_queue(b, 1) == NULL);

	ok(blkio_set_int(b, "num-queues", 2, &errmsg));

	assert(blkio_get_queue(b, -1) == NULL);
	assert(blkio_get_queue(b, 0) == NULL);
	assert(blkio_get_queue(b, 1) == NULL);

	ok(blkio_set_bool(b, "started", true, &errmsg));

	assert(blkio_get_queue(b, 0) != NULL);
	assert(blkio_get_queue(b, 1) != NULL);
	assert(blkio_get_queue(b, 3) == NULL);
	assert(blkio_get_queue(b, 2) == NULL);
	assert(blkio_get_queue(b, 10) == NULL);
	assert(blkio_get_queue(b, -1) == NULL);

	ok(blkio_set_bool(b, "started", false, &errmsg));

	assert(blkio_get_queue(b, -1) == NULL);
	assert(blkio_get_queue(b, 0) == NULL);
	assert(blkio_get_queue(b, 1) == NULL);

	ok(blkio_set_bool(b, "initialized", false, &errmsg));

	assert(blkio_get_queue(b, -1) == NULL);
	assert(blkio_get_queue(b, 0) == NULL);
	assert(blkio_get_queue(b, 1) == NULL);

	blkio_destroy(&b);
	assert(!b);

	return 0;
}
