// SPDX-License-Identifier: LGPL-2.1-or-later
#include "util.h"

int main(void)
{
	struct blkio *b;
	char *errmsg;

	err(blkio_create("foo", &b, &errmsg), -ENOENT);
	assert(!b);

	return 0;
}
