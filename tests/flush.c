// SPDX-License-Identifier: LGPL-2.1-or-later
#include <sys/mman.h>
#include "util.h"

enum {
    TEST_FILE_SIZE = 2 * 1024 * 1024,
    TEST_FILE_OFFSET = 0x10000,
};

static char filename[] = "flush-XXXXXX";

static void cleanup(void)
{
    unlink(filename);
}

/*
 * Verify that blkioq_flush() succeeds.
 */
int main(void)
{
    struct blkio *b;
    struct blkioq *q;
    struct blkio_completion completion;
    char *errmsg;
    int fd;

    ok(blkio_create("io_uring", &b, &errmsg));
    assert(b);

    register_cleanup(cleanup);
    fd = create_file(filename, TEST_FILE_SIZE);

    ok(blkio_set_int(b, "fd", fd, &errmsg));
    ok(blkio_set_bool(b, "initialized", true, &errmsg));

    fd = -1; /* ownership passed to libblkio */

    ok(blkio_set_int(b, "num-queues", 1, &errmsg));
    ok(blkio_set_bool(b, "started", true, &errmsg));

    q = blkio_get_queue(b, 0);
    assert(q);

    assert(blkioq_flush(q, NULL, 0) == 0);
    assert(blkioq_submit_and_wait(q, 1, NULL, NULL) == 0);
    assert(blkioq_get_completions(q, &completion, 1) == 1);
    assert(completion.ret == 0);

    blkio_destroy(&b);
    return 0;
}
