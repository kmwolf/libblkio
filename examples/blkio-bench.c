// SPDX-License-Identifier: MIT
/*
 * blkio-bench - block I/O benchmark
 *
 * This program measures performance statistics for a given I/O workload. This
 * is an example program that demonstrates multi-threaded event-driven I/O. For
 * real disk I/O benchmarking, use fio(1).
 *
 *   blkio-bench --num-threads=<n>
 *               --iodepth=<n>
 *               --readwrite=read|write|randread|randwrite
 *               --blocksize=<KiB>
 *               --runtime=<seconds>
 *               <driver> <property>=<value> ...
 */
#include <blkio.h>
#include <errno.h>
#include <getopt.h>
#include <inttypes.h>
#include <limits.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <time.h>
#include <unistd.h>

/* Read/write access types */
enum readwrite {
    RW_READ = 0,
    RW_WRITE,
    RW_RANDREAD,
    RW_RANDWRITE,
};

struct config {
    unsigned num_threads;
    unsigned iodepth;
    enum readwrite readwrite;
    unsigned blocksize_bytes;
    unsigned runtime_secs;
    uint64_t capacity;
};

struct io_stats {
    uint64_t count;
    uint64_t min_latency_ns;
    uint64_t max_latency_ns;
    uint64_t total_latency_ns;
};

struct thread_data {
    pthread_t thread;

    const struct config *config;

    void *full_buf; /* for all reqs in this thread */

    /* Blocking eventfds used to synchronize with the main thread */
    int init_fd;
    int start_fd;
    int stop_fd;

    struct blkioq *q;
    uint64_t offset;
    struct io_stats stats;

    struct random_data random_buf;
    char random_state[256];
};

struct io_req {
    struct timespec start;
    void *buf;
};

static uint64_t delta_ns(const struct timespec *start,
                         const struct timespec *end)
{
    static const uint64_t ns_per_sec = 1000000000ul;

    return (end->tv_sec - start->tv_sec) * ns_per_sec +
           end->tv_nsec - start->tv_nsec;
}

static void prepare_req(struct io_req *req, struct thread_data *td,
                        const struct timespec *now)
{
    enum readwrite readwrite = td->config->readwrite;
    uint64_t offset;
    unsigned blocksize_bytes = td->config->blocksize_bytes;

    req->start = *now;

    if (readwrite == RW_READ || readwrite == RW_WRITE) {
        offset = td->offset;
        td->offset = (td->offset + blocksize_bytes) % td->config->capacity;
    } else {
        int32_t val32;
        uint64_t val;

        random_r(&td->random_buf, &val32);
        val = val32;
        random_r(&td->random_buf, &val32);
        val |= (uint64_t)val32 << 32;

        offset = (val * blocksize_bytes) % td->config->capacity;
    }

    if (readwrite == RW_READ || readwrite == RW_RANDREAD) {
        blkioq_read(td->q, offset, req->buf, blocksize_bytes, req, 0);
    } else {
        blkioq_write(td->q, offset, req->buf, blocksize_bytes, req, 0);
    }
}

void *thread_main(void *opaque)
{
    struct thread_data *td = opaque;
    struct blkio_completion *completions;
    struct io_req *reqs;
    struct timespec now;
    struct epoll_event epoll_event = {
        .events = EPOLLIN,
    };
    uint64_t val = 1;
    int epfd;
    int completion_fd;

    td->offset = 0;

    completions = malloc(sizeof(*completions) * td->config->iodepth);
    if (!completions) {
        fprintf(stderr, "Failed to allocate completions\n");
        exit(EXIT_FAILURE);
    }

    reqs = malloc(sizeof(*reqs) * td->config->iodepth);
    if (!reqs) {
        fprintf(stderr, "Failed to allocated reqs\n");
        exit(EXIT_FAILURE);
    }

    /* Set request buffers */
    for (int i = 0; i < td->config->iodepth; i++) {
        reqs[i].buf = td->full_buf + i * td->config->blocksize_bytes;
    }

    epfd = epoll_create1(EPOLL_CLOEXEC);
    if (epfd < 0) {
        perror("epoll_create1");
        exit(EXIT_FAILURE);
    }

    completion_fd = blkioq_get_completion_fd(td->q);
    epoll_event.data.u32 = completion_fd;
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, completion_fd, &epoll_event) < 0) {
        perror("epoll_ctl");
        exit(EXIT_FAILURE);
    }

    epoll_event.data.u32 = td->stop_fd;
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, td->stop_fd, &epoll_event) < 0) {
        perror("epoll_ctl");
        exit(EXIT_FAILURE);
    }

    /* Notify main thread that initialization is complete */
    write(td->init_fd, &val, sizeof(val));

    /* Wait until it's time to start */
    read(td->start_fd, &val, sizeof(val));

    if (clock_gettime(CLOCK_MONOTONIC, &now) != 0) {
        perror("clock_gettime");
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < td->config->iodepth; i++) {
        prepare_req(&reqs[i], td, &now);
    }

    while (true) {
        int n;

        blkioq_submit_and_wait(td->q, 0, NULL, NULL);

        if (epoll_wait(epfd, &epoll_event, 1, -1) != 1) {
            if (errno == EINTR) {
                continue;
            }
            perror("epoll_wait");
            exit(EXIT_FAILURE);
        }

        if (epoll_event.data.u32 == td->stop_fd) {
            break;
        }

        n = blkioq_get_completions(td->q, completions, td->config->iodepth);

        if (clock_gettime(CLOCK_MONOTONIC, &now) != 0) {
            perror("clock_gettime");
            exit(EXIT_FAILURE);
        }

        for (int i = 0; i < n; i++) {
            struct io_req *req = completions[i].user_data;
            uint64_t latency_ns;

            if (completions[i].ret != td->config->blocksize_bytes) {
                fprintf(stderr, "I/O request failed: %d\n",
                        completions[i].ret);
                exit(EXIT_FAILURE);
            }

            td->stats.count++;

            latency_ns = delta_ns(&req->start, &now);
            if (latency_ns < td->stats.min_latency_ns) {
                td->stats.min_latency_ns = latency_ns;
            }
            if (latency_ns > td->stats.max_latency_ns) {
                td->stats.max_latency_ns = latency_ns;
            }
            td->stats.total_latency_ns += latency_ns;

            /* Prepare next request */
            prepare_req(req, td, &now);
        }
    }

    close(epfd);
    free(reqs);
    free(completions);
    return NULL;
}

static void thread_init(struct thread_data *td,
                        const struct config *config,
                        void *full_buf,
                        struct blkioq *q,
                        uint64_t offset)
{
    uint64_t val;
    int ret;

    td->config = config;
    td->full_buf = full_buf;
    td->q = q;
    td->offset = offset;
    memset(&td->random_buf, 0, sizeof(td->random_buf));
    initstate_r(gettid(), td->random_state, sizeof(td->random_state),
                &td->random_buf);

    memset(&td->stats, 0, sizeof(td->stats));
    td->stats.min_latency_ns = UINT64_MAX;

    td->init_fd = eventfd(0, EFD_CLOEXEC);
    td->start_fd = eventfd(0, EFD_CLOEXEC);
    td->stop_fd = eventfd(0, EFD_CLOEXEC);
    if (td->init_fd < 0 || td->start_fd < 0 || td->stop_fd < 0) {
        perror("eventfd");
        exit(EXIT_FAILURE);
    }

    ret = pthread_create(&td->thread, NULL, thread_main, td);
    if (ret != 0) {
        errno = ret;
        perror("pthread_create");
        exit(EXIT_FAILURE);
    }

    /* Wait for the thread to initialize */
    read(td->init_fd, &val, sizeof(val));
}

static void thread_cleanup(struct thread_data *td)
{
    uint64_t val = 1;

    write(td->stop_fd, &val, sizeof(val));
    pthread_join(td->thread, NULL);

    close(td->init_fd);
    close(td->start_fd);
    close(td->stop_fd);
}

static void usage(const char *progname)
{
    printf("Usage: %s --num-threads=<n>\n", progname);
    printf("          --iodepth=<n>\n");
    printf("          --readwrite=read|write|randread|randwrite\n");
    printf("          --blocksize=<KiB>\n");
    printf("          --runtime=<seconds>\n");
    printf("          <driver> <property>=<value> ...\n");
    printf("Measure performance statistics for a given I/O workload.\n");
    exit(EXIT_FAILURE);
}

static void assign_property(struct blkio *b,
                            const char *property,
                            const char *value)
{
    char *errmsg;
    int ret;

    ret = blkio_set_str(b, property, value, &errmsg);
    if (ret < 0) {
        fprintf(stderr, "Failed to assign \"%s\" to \"%s\": %s: %s\n",
                value, property, strerror(-ret), errmsg);
        free(errmsg);
        exit(EXIT_FAILURE);
    }
}

static void handle_property_option(struct blkio *b, char *option)
{
    char *property = option;
    char *value = strchr(property, '=');

    if (!value) {
        fprintf(stderr, "%s: missing property value\n", option);
        exit(EXIT_FAILURE);
    }

    /* The C standard allows argv[] string modification */
    *value = '\0';
    value++;

    assign_property(b, property, value);
}

enum {
    OPT_NUM_THREADS = 256,
    OPT_IODEPTH,
    OPT_READWRITE,
    OPT_BLOCKSIZE,
    OPT_RUNTIME,
};

static const struct option options[] = {
    {
        .name = "help",
        .has_arg = no_argument,
        .flag = NULL,
        .val = 'h',
    },
    {
        .name = "num-threads",
        .has_arg = required_argument,
        .flag = NULL,
        .val = OPT_NUM_THREADS,
    },
    {
        .name = "iodepth",
        .has_arg = required_argument,
        .flag = NULL,
        .val = OPT_IODEPTH,
    },
    {
        .name = "readwrite",
        .has_arg = required_argument,
        .flag = NULL,
        .val = OPT_READWRITE,
    },
    {
        .name = "blocksize",
        .has_arg = required_argument,
        .flag = NULL,
        .val = OPT_BLOCKSIZE,
    },
    {
        .name = "runtime",
        .has_arg = required_argument,
        .flag = NULL,
        .val = OPT_RUNTIME,
    },
    {}
};

int main(int argc, char **argv)
{
    struct config config = {
        .num_threads = 1,
        .iodepth = 1,
        .readwrite = RW_READ,
        .blocksize_bytes = 4096,
        .runtime_secs = 30,
    };
    struct blkio *b = NULL;
    struct blkio_mem_region mem_region;
    struct thread_data *tds;
    const char *driver;
    char *errmsg;
    unsigned remaining_time;
    int opt;
    int ret;

    while ((opt = getopt_long(argc, argv, "h", options, NULL)) != -1) {
        switch (opt) {
        case 'h':
            usage(argv[0]);
            break;

        case OPT_NUM_THREADS: {
            unsigned long val = strtoul(optarg, NULL, 10);
            if (val > (unsigned long)UINT_MAX || val == 0) {
                fprintf(stderr, "Invalid number of threads\n");
                usage(argv[0]);
            }
            config.num_threads = val;
        } break;

        case OPT_IODEPTH: {
            unsigned long val = strtoul(optarg, NULL, 10);
            if (val > (unsigned long)UINT_MAX || val == 0) {
                fprintf(stderr, "Invalid iodepth\n");
                usage(argv[0]);
            }
            config.iodepth = val;
        } break;

        case OPT_READWRITE:
            if (strcmp(optarg, "read") == 0) {
                config.readwrite = RW_READ;
            } else if (strcmp(optarg, "write") == 0) {
                config.readwrite = RW_WRITE;
            } else if (strcmp(optarg, "randread") == 0) {
                config.readwrite = RW_RANDREAD;
            } else if (strcmp(optarg, "randwrite") == 0) {
                config.readwrite = RW_RANDWRITE;
            } else {
                fprintf(stderr, "Invalid readwrite option\n");
                usage(argv[0]);
            }
            break;

        case OPT_BLOCKSIZE: {
            unsigned long val = strtoul(optarg, NULL, 10);
            if (val > (unsigned long)UINT_MAX || val == 0) {
                fprintf(stderr, "Invalid blocksize\n");
                usage(argv[0]);
            }
            config.blocksize_bytes = val * 1024;
        } break;

        case OPT_RUNTIME: {
            unsigned long val = strtoul(optarg, NULL, 10);
            if (val > (unsigned long)UINT_MAX || val == 0) {
                fprintf(stderr, "Invalid runtime\n");
                usage(argv[0]);
            }
            config.runtime_secs = val;
        } break;

        default:
            usage(argv[0]);
            break;
        }
    }

    if (optind == argc) {
        fprintf(stderr, "Missing blkio driver name argument\n");
        usage(argv[0]);
    }

    driver = argv[optind++];
    ret = blkio_create(driver, &b, &errmsg);
    if (ret < 0) {
        fprintf(stderr, "Failed to create blkio driver \"%s\": %s\n",
                driver, errmsg);
        free(errmsg);
        return EXIT_FAILURE;
    }

    while (optind < argc) {
        handle_property_option(b, argv[optind++]);
    }

    assign_property(b, "initialized", "true");

    ret = blkio_get_uint64(b, "capacity", &config.capacity, &errmsg);
    if (ret != 0) {
        fprintf(stderr, "Unable to get device capacity: %s\n",
                errmsg);
        free(errmsg);
        return EXIT_FAILURE;
    }

    ret = blkio_set_int(b, "num-queues", config.num_threads, &errmsg);
    if (ret != 0) {
        fprintf(stderr, "Failed to set num-queues: %s\n", errmsg);
        free(errmsg);
        return EXIT_FAILURE;
    }

    /*
     * The max-descs property could be set to accomodate iodepth, but we assume
     * that is not necessary.
     */

    assign_property(b, "started", "true");

    ret = blkio_alloc_mem_region(b, &mem_region,
            config.num_threads * config.iodepth * config.blocksize_bytes,
            0, &errmsg);
    if (ret != 0) {
        fprintf(stderr, "Failed to allocate I/O buffer memory: %s\n",
                errmsg);
        free(errmsg);
        return EXIT_FAILURE;
    }

    tds = malloc(config.num_threads * sizeof(tds[0]));
    if (!tds) {
        perror("malloc");
        return EXIT_FAILURE;
    }

    for (unsigned i = 0; i < config.num_threads; i++) {
        void *full_buf = mem_region.addr + i * config.iodepth *
                         config.blocksize_bytes;
        struct blkioq *q = blkio_get_queue(b, i);
        uint64_t offset = config.capacity / config.num_threads * i;

        thread_init(&tds[i], &config, full_buf, q, offset);
    }

    for (unsigned i = 0; i < config.num_threads; i++) {
        uint64_t val = 1;

        write(tds[i].start_fd, &val, sizeof(val));
    }

    remaining_time = sleep(config.runtime_secs);
    if (remaining_time > 0) {
        fprintf(stderr, "Stopped early by a signal...\n");
        config.runtime_secs -= remaining_time;
    }

    for (unsigned i = 0; i < config.num_threads; i++) {
        struct thread_data *td = &tds[i];

        thread_cleanup(td);

        printf("Thread %u IOPS: %" PRIu64 " Min/Mean/Max Latency (ns): %" PRIu64 "/%" PRIu64 "/%" PRIu64 "\n",
               i,
               td->stats.count / config.runtime_secs,
               td->stats.min_latency_ns,
               td->stats.total_latency_ns / td->stats.count,
               td->stats.max_latency_ns);
    }

    free(tds);
    if (blkio_del_mem_region(b, &mem_region, &errmsg) != 0) {
        free(errmsg);
    }
    blkio_destroy(&b);
    return EXIT_SUCCESS;
}
