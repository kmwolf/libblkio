// SPDX-License-Identifier: LGPL-2.1-or-later
/*
 * Block device I/O library
 * Copyright (C) 2020 Red Hat, Inc.
 *
 * See blkio(3) for API documentation.
 */
#ifndef BLKIO_H
#define BLKIO_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <sys/uio.h>

struct blkio;
struct blkioq;

struct blkio_mem_region
{
	void *addr;
	size_t len;
	off_t fd_offset;
	int fd;
};

struct blkio_completion
{
	void *user_data;
	int ret;
};

int blkio_create(const char *driver, struct blkio **bp, char **errmsg);
void blkio_destroy(struct blkio **bp);

int blkio_get_bool(struct blkio *b, const char *name, bool *value, char **errmsg);
int blkio_get_int(struct blkio *b, const char *name, int *value, char **errmsg);
int blkio_get_uint64(struct blkio *b, const char *name, uint64_t *value, char **errmsg);
int blkio_get_str(struct blkio *b, const char *name, char **value, char **errmsg);

int blkio_set_bool(struct blkio *b, const char *name, bool value, char **errmsg);
int blkio_set_int(struct blkio *b, const char *name, int value, char **errmsg);
int blkio_set_uint64(struct blkio *b, const char *name, uint64_t value, char **errmsg);
int blkio_set_str(struct blkio *b, const char *name, const char *value, char **errmsg);

enum {
    BLKIO_MEM_CLOEXEC = 1 << 0,
};

int blkio_alloc_mem_region(struct blkio *b, struct blkio_mem_region *region, size_t len, uint32_t flags, char **errmsg);
void blkio_free_mem_region(struct blkio *b, const struct blkio_mem_region *region);
int blkio_add_mem_region(struct blkio *b,
                         const struct blkio_mem_region *region,
                         char **errmsg);
int blkio_del_mem_region(struct blkio *b,
                         const struct blkio_mem_region *region,
                         char **errmsg);

struct blkioq *blkio_get_queue(struct blkio *b, int index);

enum {
    BLKIO_REQ_POLLED = 1 << 0,
    BLKIO_REQ_FUA = 1 << 1,
};

int blkioq_read(struct blkioq *q, uint64_t start, void *buf, size_t len, void *user_data, uint32_t flags);
int blkioq_write(struct blkioq *q, uint64_t start, const void *buf, size_t len, void *user_data, uint32_t flags);
int blkioq_readv(struct blkioq *q, uint64_t start, const struct iovec *iovec, int iovcnt, void *user_data, uint32_t flags);
int blkioq_writev(struct blkioq *q, uint64_t start, struct iovec *iovec, size_t iovcnt, void *user_data, uint32_t flags);
int blkioq_write_zeroes(struct blkioq *q, uint64_t start, uint64_t len, void *user_data, uint32_t flags);
int blkioq_discard(struct blkioq *q, uint64_t start, uint64_t len, void *user_data, uint32_t flags);
int blkioq_flush(struct blkioq *q, void *user_data, uint32_t flags);

int blkioq_submit_and_wait(struct blkioq *q, int min_completions, const struct timespec *timeout, const sigset_t *sig);
int blkioq_get_completions(struct blkioq *q, struct blkio_completion *completions, int count);
int blkioq_get_completion_fd(struct blkioq *q);

#ifdef __cplusplus
}
#endif

#endif /* BLKIO_H */
