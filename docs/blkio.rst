=====
blkio
=====
------------------------
Block device I/O library
------------------------
:Manual section: 3
:Author: Stefan Hajnoczi <stefanha@redhat.com>

DESCRIPTION
-----------
libblkio is a library for accessing data stored on *block devices*. Block
devices offer persistent data storage and are addressable in fixed-size units
called *blocks*. Block sizes of 4 KiB or 512 bytes are typical. Hard disk
drives, solid state disks (SSDs), USB mass storage devices, and other types of
hardware are block devices.

The focus of libblkio is on fast I/O for multi-threaded applications.
Management of block devices, including partitioning and resizing, is outside
the scope of the library.

Block devices have one or more *queues* for submitting I/O requests such as
reads and writes. Block devices process I/O requests from their queues and
produce a return code for each completed request indicating success or an
error.

The application is responsible for thread-safety. No thread synchronization is
necessary when a queue is only used from a single thread. Proper
synchronization is required when sharing a queue between multiple threads.

libblkio can be used in blocking, event-driven, and polling modes depending on
the architecture of the application and its performance requirements.

*Blocking mode* suspends the execution of the current thread until the request
completes. This is most natural way of writing programs that perform a sequence
of I/O requests but cannot exploit request parallelism.

*Event-driven mode* provides a completion file descriptor that the application
can monitor from its event loop. This allows multiple I/O requests to be in
flight simultaneously and the application can respond to other events while
waiting for completions.

*Polling mode* also supports multiple in-flight requests but the application
continuously checks for completions, typically from a tight loop, in order to
minimize latency.

libblkio contains *drivers* for several block I/O interfaces. This allows
applications using libblkio to access different block devices through a single
API.

Creating a `blkio` instance
~~~~~~~~~~~~~~~~~~~~~~~~~~~
A `struct blkio` instance is created from a specific driver such as "io_uring"
as follows::

  struct blkio *b;
  char *errmsg;
  int ret;

  ret = blkio_create("io_uring", &b, &errmsg);
  if (ret < 0) {
      fprintf(stderr, "%s: %s\n", strerror(-ret), errmsg);
      free(errmsg);
      return;
  }

For a list of available drivers, see the DRIVERS_ section below.

Error messages
~~~~~~~~~~~~~~
Functions generally return 0 on success and a negative `errno(3)` value on
failure.

Functions that take a `char **errmsg` argument assign NULL to `*errmsg` on
success or the address of an allocated error message string on error. In that
case the caller must deallocate the error message using `free(3)`. Callers that
do not require an error message can pass a `NULL` `errmsg` argument to avoid
the responsibility of deallocation.

Connecting to a block device
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Connection details for a block device are specified by setting properties on
the `blkio` instance. The available properties depend on the driver. For
example, the io_uring driver's "path" property is set to `/dev/sdb` to access a
local disk::

  char *errmsg;
  int ret = blkio_set_str(b, "path", "/dev/sdb", &errmsg);
  if (ret < 0) {
      fprintf(stderr, "%s: %s\n", strerror(-ret), errmsg);
      free(errmsg);
      blkio_destroy(&b);
      return;
  }

Once the connection details have been specified the `blkio` instance can be
connected to the block device by setting the "initialized" property to `true`::

  ret = blkio_set_bool(b, "initialized", true, &errmsg);

Configuring the block device
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
After the `blkio` instance is initialized, properties are available to configure
its operation and query device characteristics such as the maximum number of
queues. See PROPERTIES_ for details.

For example, the number of queues can be set as follows::

  ret = blkio_set_int(b, "num-queues", 4, &errmsg);

Once configuration is complete the `blkio` instance is started by setting the
"started" property to `true`::

  ret = blkio_set_bool(b, "started", true, &errmsg);

Adding memory regions
~~~~~~~~~~~~~~~~~~~~~
Memory used for I/O data buffers must be registered before submitting requests
that touch the memory. Memory regions are registered globally for the `blkio`
instance and are available to all queues. A memory region is represented as
follows::

  struct blkio_mem_region
  {
      void *addr;
      size_t len;
      off_t fd_offset;
      int fd;
  };

The `addr` field contains the starting address of the memory region. Requests
transfer data between the block device and a subset of the memory region,
including up to the entire memory region. Multiple requests can access the same
memory region simultaneously, although usually with non-overlapping areas.

The `len` field is the size of the memory region in bytes.

The `fd` field is the file descriptor for the memory region. Some drivers
require that I/O data buffers are located in file-backed memory. This can be
anonymous memory from `memfd_create(2)` rather than an actual file on disk.
This field may be -1 if the driver does not require shared memory.

The `fd_offset` field is the byte offset from the start of the file given in
`fd`.

The application can either allocate I/O data buffers itself and describe them
with `struct blkio_mem_region` or it can use `blkio_alloc_mem_region()` and
`blkio_free_mem_region()` to allocate memory suitable for I/O data buffers::

  int blkio_alloc_mem_region(struct blkio *b, struct blkio_mem_region *region,
                             size_t len, uint32_t flags, char **errmsg);
  int blkio_free_mem_region(struct blkio *b,
                            const struct blkio_mem_region *region);

The `len` argument is the number of bytes to allocate. The `flags` argument
must be zero.

Memory regions can be added and deleted after the `blkio` instance has been
started using the `blkio_add_mem_region()` and `blkio_del_mem_region()`
functions::

  int blkio_add_mem_region(struct blkio *b,
                           const struct blkio_mem_region *region,
                           char **errmsg);
  int blkio_del_mem_region(struct blkio *b,
                           const struct blkio_mem_region *region,
                           char **errmsg);

These functions must not be called while requests are in flight that access the
affected memory region. Memory regions must not overlap.

For best performance applications should add memory regions once and reuse them
instead of changing memory regions frequently.

Memory regions are cleared when the `blkio` instance is stopped.

Performing I/O
~~~~~~~~~~~~~~
Once at least one memory region has been added, the queues are ready for
request processing. The following example reads 4096 bytes from byte offset
0x10000::

  struct blkioq *q = blkio_get_queue(b, 0);

  ret = blkioq_read(q, 0x10000, buf, buf_size, NULL, 0);
  if (ret < 0) ...

  ret = blkioq_submit_and_wait(q, 1, NULL, NULL);
  if (ret != 0) ...

  struct blkio_completion completion;
  ret = blkioq_get_completions(q, &completion, 1);
  if (ret != 1) ...
  if (completion.ret != 0) ...

This is an example of blocking mode where the thread waits for I/O to complete.

The `blkioq_submit_and_wait()` function offers the following arguments::

  int blkioq_submit_and_wait(struct blkioq *q, int min_completions,
                             const struct timespec *timeout,
                             const sigset_t *sig);

The `min_completions` argument controls how many completions to wait for. A
value greater than 0 causes the function to block until the number of
completions has been reached. A value of 0 causes the function to return
immediately after submitting I/O without waiting.

The `timeout` argument specifies the maximum amount of time to wait for
completions. The function returns -ETIME if the timeout expires before a
request completes. If `timeout` is NULL the function blocks indefinitely.

The `sig` argument temporarily sets the signal mask of the process while
waiting for completions. The signal mask is unchanged if the argument is NULL.
Changing the signal mask allows the thread to be woken by a signal without race
conditions.

The `blkioq_submit_and_wait()` example above blocks until one request has
completed because the `min_completions` argument is 1. See below for details on
event-driven and polling modes.

Completions are represented by `struct blkio_completion`::

  struct blkio_completion
  {
      void *user_data;
      int ret;
  };

The `user_data` field is the same pointer passed to `blkioq_read()` in the
example above. Applications that submit multiple requests can use `user_data`
to correlate completions to previously submitted requests.

The `ret` field is the return code for the I/O request in negative errno
representation. This field is 0 on success.

The `blkioq_get_completions()` function returns available completions::

  int blkioq_get_completions(struct blkioq *q,
                             struct blkio_completion *completions,
                             int count);

The caller provides an array of one or more completions. The function returns
the number of completions that have been filled in or 0 if no completions are
available.

Event-driven mode
~~~~~~~~~~~~~~~~~
Completion processing can be integrated into the event loop of an application.
Each queue has a completion file descriptor that becomes readable when
`blkioq_get_completions()` needs to be called again::

  int blkioq_get_completion_fd(struct blkioq *q);

The returned file descriptor has O_NONBLOCK set. The application may switch the
file descriptor to blocking mode.

The application must read 8 bytes from the completion file descriptor to reset
the event before calling `blkioq_get_completions()`. The contents of the bytes
are undefined and should not be interpreted by the application.

The following example demonstrates event-driven I/O::

  struct blkioq *q = blkio_get_queue(b, 0);
  int completion_fd = blkio_get_completion_fd(q);
  char event_data[8];

  /* Switch to blocking mode for read(2) below */
  fcntl(completion_fd, F_SETFL,
        fcntl(completion_fd, F_GETFL, NULL) & ~O_NONBLOCK);

  ret = blkioq_read(q, 0x10000, buf, buf_size, NULL, 0);
  if (ret < 0) ...

  /* Since min_completions = 0 we will submit but not wait */
  ret = blkioq_submit_and_wait(q, 0, NULL, NULL);
  if (ret != 0) ...

  /* Wait for the next event on the completion file descriptor */
  read(completion_fd, event_data, sizeof(event_data));

  struct blkio_completion completion;
  ret = blkioq_get_completions(q, &completion, 1);
  if (ret != 1) ...
  if (completion.ret != 0) ...

This example uses a blocking `read(2)` to wait and consume the next event on
the completion file descriptor. Normally `completion_fd` would be registered
with an event loop so the application can perform other tasks while waiting.

Polling mode
~~~~~~~~~~~~
Waiting for completions using `blkioq_submit_and_wait()` with `min_completions`
> 0 can cause the current thread to be descheduled by the operating system's
scheduler. The same is true when waiting for events on the completion file
descriptor returned by `blkioq_get_completion_fd()`. Some applications require
consistent low response times and therefore cannot risk being descheduled.

`blkioq_get_completions()` may be called from a CPU polling loop to check for
completions::

  struct blkioq *q = blkio_get_queue(b, 0);

  ret = blkioq_read(q, 0x10000, buf, buf_size, NULL, BLKIO_REQ_POLLED);
  if (ret < 0) ...

  /* Since min_completions = 0 we will submit but not wait */
  ret = blkioq_submit_and_wait(q, 0, NULL, NULL);
  if (ret != 0) ...

  /* Busy-wait for the completion */
  struct blkio_completion completion;
  do {
      ret = blkioq_get_completions(q, &completion, 1));
  } while (ret == 0);

  if (ret != 1) ...
  if (completion.ret != 0) ...

The `BLKIO_REQ_POLLED` request flag is a hint to suppress completion events.
Events are not needed since `blkioq_get_completions()` is being checked from
the polling loop.

Request types
~~~~~~~~~~~~~
The following types of I/O requests are available::

  int blkioq_read(struct blkioq *q, uint64_t start, void *buf, size_t len,
                  void *user_data, uint32_t flags);
  int blkioq_write(struct blkioq *q, uint64_t start, void *buf, size_t len,
                   void *user_data, uint32_t flags);
  int blkioq_readv(struct blkioq *q, uint64_t start, struct iovec *iovec,
                   size_t iovcnt, void *user_data, uint32_t flags);
  int blkioq_writev(struct blkioq *q, uint64_t start, struct iovec *iovec,
                    size_t iovcnt, void *user_data, uint32_t flags);
  int blkioq_write_zeroes(struct blkioq *q, uint64_t start, uint64_t len,
                          void *user_data, uint32_t flags);
  int blkioq_discard(struct blkioq *q, uint64_t start, uint64_t len,
                     void *user_data, uint32_t flags);
  int blkioq_flush(struct blkioq *q, void *user_data, uint32_t flags);

The block device may see requests as soon as they these functions are called,
but `blkioq_submit_and_wait()` must be called to ensure requests are seen.

Memory buffers pointed to by `buf` and `iovec` must be within regions
registered using `blkio_add_mem_region()`.

`blkioq_read()` and `blkioq_readv()` read data from the block device at byte
offset `start`. `blkioq_write()` and `blkioq_writev()` write data to the block
device at byte offset `start`. The length of the buffer is `len` bytes and the
total size of the `iovec` elements, respectively.

TODO alignment requirements of `start` and the length

TODO `blkioq_write_zeroes()`
TODO `blkioq_discard()`

`blkioq_flush()` persists completed writes to the storage medium. Data is
persistent once the flush request completes successfully. Applications that
need to ensure that data persists across power failure or crash must submit
flush requests at appropriate points.

The `user_data` pointer is returned in the `struct blkio_completion::user_data`
field by `blkioq_get_completions()`. It allows applications to correlate a
completion with its request.

If the queue is full these functions return -ENOBUFS.

No ordering guarantees are defined for requests that are in flight
simultaneously. For example, a flush request is not guaranteed to persist
in-flight write requests. Instead the application must wait for write requests
that it wishes to persist to complete before calling `blkioq_flush()`.

Similarly, there are no ordering guarantees between multiple queues of a block
device. Multi-threaded applications that rely on an ordering between multiple
queues must wait for the first request to complete on one queue, synchronize
threads as needed, and then submit the second request on the other queue.

Request flags
`````````````
The following request flags are available:

BLKIO_REQ_POLLED
  Hint to suppress completion events. The caller is using
  `blkioq_get_completions()` from a polling loop and does not require
  completion events delivered through the completion file descriptor or
  blocking `blkioq_submit_and_wait()` behavior. This is a performance
  optimization.

BLKIO_REQ_FUA
  Ensures that data written by this request reaches persistent storage before
  the request is completed. This is also known as Full Unit Access (FUA). This
  flag eliminates the need for a separate `blkioq_flush()` call after the
  request has completed. Other data that was previously successfully written
  without the `BLKIO_REQ_FUA` flag is not necessarily persisted by this flag as
  it is only guaranteed to affect the current request. Supported by
  `blkioq_write()` and `blkioq_writev()`.
  TODO blkioq_write_zeroes() and blkioq_discard()?

PROPERTIES
----------
The configuration of `blkio` instances is done through property accesses. Each
property has a name and a type (bool, int, str, uint64). Properties may be
read-only (r), write-only (w), or read/write (rw).

Access to properties depends on the `blkio` instance state
(created/initialized/started). A property may be read/write in the initialized
state but read-only in the started state. This is written as "rw initialized, r
started".

The following properties APIs are available::

  int blkio_get_bool(struct blkio *b, const char *name, bool *value, char **errmsg);
  int blkio_get_int(struct blkio *b, const char *name, int *value, char **errmsg);
  int blkio_get_uint64(struct blkio *b, const char *name, uint64_t *value, char **errmsg);
  int blkio_get_str(struct blkio *b, const char *name, char **value, char **errmsg);

  int blkio_set_bool(struct blkio *b, const char *name, bool value, char **errmsg);
  int blkio_set_int(struct blkio *b, const char *name, int value, char **errmsg);
  int blkio_set_uint64(struct blkio *b, const char *name, uint64_t value, char **errmsg);
  int blkio_set_str(struct blkio *b, const char *name, const char *value, char **errmsg);

`blkio_get_str()` assigns to `*value` and the caller must use `free(3)` to
deallocate the memory.

`blkio_get_str()` automatically converts to string representation if the
property is not a str. `blkio_set_str()` automatically converts from string
representation if the property is not a str. This can be used to easily fetch
values from and store values to an application's text-based configuration file
or command-line. Aside from this automatic conversion, the other property APIs
fail with ENOTTY if the property does not have the right type.

The following properties are common across all drivers. Driver-specific
properties are documented in DRIVERS_.

capacity (uint64, r initialized/started)
  The size of the block device in bytes.

driver (str, r created/initialized/started)
  The driver name that was passed to `blkio_create()`. See DRIVERS_ for details
  on available drivers.

initialized (bool, rw created/initialized, r started)
  Set this to true to connect to the block device. Driver-specific
  properties should have been previously set to specify the connection details
  to the block device. After this property has been set to true the `blkio`
  instance is in the initialized state.

max-queues (int, r initialized/started)
  The maximum number of queues.

max-descs (int, r initialized/started)
  The maximum number of descriptors per queue.

num-queues (int, rw initialized, r started)
  The number of queues. The default is 1.

num-descs (int, rw initialized, r started)
  The number of descriptors per queue. The default depends on the driver and is
  sufficient for most applications. Tuning this value can affect performance.

  TODO define the meaning of this (iovecs, requests, or some other concept).

started (bool, r created, rw initialized/started)
  Set this to true to activate the queues. The `blkio` instance must
  be in the initialized state. After this property has been set to true the
  `blkio` instance is in the started state and I/O requests may be submitted on
  its queues.

DRIVERS
-------

io_uring
~~~~~~~~
The io_uring driver uses the Linux io_uring system call interface to perform
I/O on files and block device nodes. Both regular files and block device nodes
are supported.

Driver properties
`````````````````
direct (bool, rw created, r initialized/started)
  True to bypass the page cache with O_DIRECT. The default is false.

fd (int, rw created, r initialized/started)
  An existing open file descriptor for the file or block device node. Ownership
  of the file descriptor is passed to the library when `initialized` is
  successfully set to true.

  Note that `direct`, `readonly`, etc have no effect and it is the user's
  responsibility to open the file with the desired flags.

path (str, rw created, r initialized/started)
  The file system path of the file or block device node.

readonly (bool, rw created, r initialized/started)
  True if write requests are not allowed. The default is false.

BUILD SYSTEM INTEGRATION
------------------------

pkg-config is the recommended way to build a program with libblkio::

  $ cc -o app app.c `pkg-config blkio --cflags --libs`

Meson projects can use pkg-config as follows::

  blkio = dependency('blkio')
  executable('app', 'app.c', dependencies : [blkio])

FREQUENTLY ASKED QUESTIONS
--------------------------
Can network storage drivers be added?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Maybe. The API was designed with a synchronous control path. Functions like
`blkio_get_uint64()` must return quickly. Operations on network storage can
take an unbounded amount of time (in the absence of a timeout mechanism) and
are not a good fit for synchronous APIs. A more complex asynchronous control
path API could be added for applications wishing to use network storage drivers
in the future.

Can non-Linux operating systems be supported in the future?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Maybe. No attempt has been made to restrict the library to POSIX features only
and most drivers are platform-specific. If there is demand for supporting other
operating systems and developers willing to work on it then it may be possible.

Can a Linux AIO driver be added?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Linux AIO could serve as a fallback on systems where io_uring is not available.
However, `io_submit(2)` can block the process and this causes performance
problems in event-driven applications that require that the event loop does not
block. Unless Linux AIO is fixed it is unlikely that a proposal to add a driver
will be accepted.

SEE ALSO
--------
io_uring_setup(2), io_setup(2), aio(7)
