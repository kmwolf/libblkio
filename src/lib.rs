mod blkio;
mod iouring;
mod properties;

use libc::{
    close, ftruncate, iovec, mmap, munmap, off_t, sigset_t, size_t, syscall, timespec,
    SYS_memfd_create, EINVAL, ENOENT, MAP_FAILED, MAP_SHARED, MFD_CLOEXEC, PROT_READ, PROT_WRITE,
};
use std::ffi::{CStr, CString};
use std::fs::File;
use std::os::raw::{c_char, c_int, c_void};
use std::os::unix::io::{FromRawFd, IntoRawFd};
use std::ptr;
use std::time::Duration;

use blkio::{Blkio, Blkioq, Error, Result};
use iouring::IoUring;

// Returns a libc heap-allocated string or null
unsafe fn unwrap_and_strdup(value: Option<String>) -> *mut c_char {
    match value {
        Some(msg) => match CString::new(msg) {
            Ok(c_msg) => libc::strdup(c_msg.as_ptr()),
            _ => ptr::null_mut(),
        },
        _ => ptr::null_mut(),
    }
}

// Consume a Result and produce its error message
fn result_to_errmsg<T>(result: Result<T>) -> Option<String> {
    result.err().map(|e| e.1)
}

// Set @errmsg from last errno and return -errno
fn set_errmsg_from_errno(errmsg: *mut *mut c_char) -> c_int {
    let err = Error::from_io_error(std::io::Error::last_os_error(), EINVAL);
    let neg_errno = err.0;
    unsafe {
        if !errmsg.is_null() {
            *errmsg = unwrap_and_strdup(result_to_errmsg::<()>(Err(err)));
        }
    }
    neg_errno
}

// Set @errmsg from @result and return 0 or -errno
fn set_errmsg_from_result<T>(errmsg: *mut *mut c_char, result: Result<T>) -> c_int {
    let ret = match result {
        Ok(_) => 0,
        Err(Error(r, _)) => r,
    };

    if !errmsg.is_null() {
        unsafe {
            *errmsg = unwrap_and_strdup(result_to_errmsg(result));
        }
    }

    ret
}

// Set @errmsg from @err and return -errno
fn set_errmsg(errmsg: *mut *mut c_char, err: Error) -> c_int {
    unsafe {
        if !errmsg.is_null() {
            *errmsg = unwrap_and_strdup(Some(err.1))
        }
    }
    err.0
}

// TODO figure out how to use type signature with bp
#[no_mangle]
pub extern "C" fn blkio_create(
    cdriver: *const c_char,
    bp: *mut *mut c_void,
    errmsg: *mut *mut c_char,
) -> c_int {
    if bp.is_null() {
        return set_errmsg(errmsg, Error(-EINVAL, "bp must be non-NULL".to_string()));
    }
    unsafe { *bp = ptr::null_mut() };

    let driver_cstr = unsafe { CStr::from_ptr(cdriver) };
    let name = match driver_cstr.to_str() {
        Ok(name) => name,
        Err(_) => return set_errmsg(errmsg, Error(-EINVAL, "Invalid property name".to_string())),
    };

    let b = match name {
        "io_uring" => Blkio::new(Box::new(IoUring::new())),
        _ => return set_errmsg(errmsg, Error(-ENOENT, "Unknown driver name".to_string())),
    };

    unsafe {
        *bp = Box::into_raw(Box::new(b)) as *mut c_void;
        if !errmsg.is_null() {
            *errmsg = ptr::null_mut();
        }
    };
    0
}

#[no_mangle]
pub extern "C" fn blkio_destroy(bp: *mut Option<Box<Blkio>>) {
    if !bp.is_null() {
        unsafe { *bp = None }
    }
}

#[no_mangle]
pub extern "C" fn blkio_get_bool(
    b: &Blkio,
    cname: *const c_char,
    value: *mut bool,
    errmsg: *mut *mut c_char,
) -> c_int {
    let name_cstr = unsafe { CStr::from_ptr(cname) };
    let result = match name_cstr.to_str() {
        Ok(name) => b.get_bool(name),
        Err(_) => Err(Error(-EINVAL, "Invalid property name".to_string())),
    };

    unsafe {
        *value = match result {
            Ok(x) => x,
            _ => false,
        };
    }

    set_errmsg_from_result(errmsg, result)
}

#[no_mangle]
pub extern "C" fn blkio_get_int(
    b: &Blkio,
    cname: *const c_char,
    value: *mut c_int,
    errmsg: *mut *mut c_char,
) -> c_int {
    let name_cstr = unsafe { CStr::from_ptr(cname) };
    let result = match name_cstr.to_str() {
        Ok(name) => b.get_i32(name),
        Err(_) => Err(Error(-EINVAL, "Invalid property name".to_string())),
    };

    unsafe {
        *value = match result {
            Ok(x) => x,
            _ => 0,
        };
    }

    set_errmsg_from_result(errmsg, result)
}

#[no_mangle]
pub extern "C" fn blkio_get_str(
    b: &Blkio,
    cname: *const c_char,
    value: *mut *mut c_char,
    errmsg: *mut *mut c_char,
) -> c_int {
    let name_cstr = unsafe { CStr::from_ptr(cname) };

    let result = match name_cstr.to_str() {
        Ok(name) => b.get_str(name),
        Err(_) => Err(Error(-EINVAL, "Invalid property name".to_string())),
    };

    let (ret, maybe_value, maybe_errmsg) = match result {
        Ok(x) => (0, Some(x), None),
        Err(Error(r, msg)) => (r, None, Some(msg)),
    };

    unsafe {
        *value = match maybe_value {
            Some(x) => match CString::new(x) {
                Ok(c_x) => libc::strdup(c_x.as_ptr()),
                Err(_) => ptr::null_mut(),
            },
            _ => ptr::null_mut(),
        };

        if !errmsg.is_null() {
            *errmsg = unwrap_and_strdup(maybe_errmsg);
        }
    }

    ret
}

#[no_mangle]
pub extern "C" fn blkio_get_uint64(
    b: &Blkio,
    cname: *const c_char,
    value: *mut u64,
    errmsg: *mut *mut c_char,
) -> c_int {
    let name_cstr = unsafe { CStr::from_ptr(cname) };
    let result = match name_cstr.to_str() {
        Ok(name) => b.get_u64(name),
        Err(_) => Err(Error(-EINVAL, "Invalid property name".to_string())),
    };

    unsafe {
        *value = match result {
            Ok(x) => x,
            _ => 0,
        };
    }

    set_errmsg_from_result(errmsg, result)
}

#[no_mangle]
pub extern "C" fn blkio_set_bool(
    b: &mut Blkio,
    cname: *const c_char,
    value: bool,
    errmsg: *mut *mut c_char,
) -> c_int {
    let name_cstr = unsafe { CStr::from_ptr(cname) };
    let result = match name_cstr.to_str() {
        Ok(name) => b.set_bool(name, value),
        Err(_) => Err(Error(-EINVAL, "Invalid property name".to_string())),
    };

    set_errmsg_from_result(errmsg, result)
}

#[no_mangle]
pub extern "C" fn blkio_set_int(
    b: &mut Blkio,
    cname: *const c_char,
    value: c_int,
    errmsg: *mut *mut c_char,
) -> c_int {
    let name_cstr = unsafe { CStr::from_ptr(cname) };
    let result = match name_cstr.to_str() {
        Ok(name) => b.set_i32(name, value),
        Err(_) => Err(Error(-EINVAL, "Invalid property name".to_string())),
    };

    set_errmsg_from_result(errmsg, result)
}

#[no_mangle]
pub extern "C" fn blkio_set_str(
    b: &mut Blkio,
    cname: *const c_char,
    cvalue: *const c_char,
    errmsg: *mut *mut c_char,
) -> c_int {
    let name_cstr = unsafe { CStr::from_ptr(cname) };
    let result = match name_cstr.to_str() {
        Ok(name) => {
            let value_cstr = unsafe { CStr::from_ptr(cvalue) };
            match value_cstr.to_str() {
                Ok(value) => b.set_str(name, value),
                Err(_) => Err(Error(-EINVAL, "Invalid value string".to_string())),
            }
        }
        Err(_) => Err(Error(-EINVAL, "Invalid property name".to_string())),
    };

    set_errmsg_from_result(errmsg, result)
}

#[no_mangle]
pub extern "C" fn blkio_set_uint64(
    b: &mut Blkio,
    cname: *const c_char,
    value: u64,
    errmsg: *mut *mut c_char,
) -> c_int {
    let name_cstr = unsafe { CStr::from_ptr(cname) };
    let result = match name_cstr.to_str() {
        Ok(name) => b.set_u64(name, value),
        Err(_) => Err(Error(-EINVAL, "Invalid property name".to_string())),
    };

    set_errmsg_from_result(errmsg, result)
}

#[repr(C)]
pub struct MemRegion {
    addr: *mut c_void,
    len: size_t,
    fd_offset: off_t,
    fd: c_int,
}

#[no_mangle]
pub extern "C" fn blkio_alloc_mem_region(
    _b: &mut Blkio,
    region: &mut MemRegion,
    len: size_t,
    _flags: u32,
    errmsg: *mut *mut c_char,
) -> c_int {
    let fd = unsafe { syscall(SYS_memfd_create, "libblkio-buf".as_ptr(), MFD_CLOEXEC) } as i32;
    if fd < 0 {
        return set_errmsg_from_errno(errmsg);
    }

    // Automatically close the fd in error code paths
    let file = unsafe { File::from_raw_fd(fd) };

    if unsafe { ftruncate(fd, len as off_t) } < 0 {
        return set_errmsg_from_errno(errmsg);
    }

    let addr = unsafe {
        mmap(
            std::ptr::null_mut(),
            len,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            fd,
            0,
        )
    };
    if addr == MAP_FAILED {
        return set_errmsg_from_errno(errmsg);
    }

    region.addr = addr;
    region.len = len;
    region.fd_offset = 0;
    region.fd = file.into_raw_fd();
    unsafe {
        if !errmsg.is_null() {
            *errmsg = std::ptr::null_mut();
        }
    }
    0
}

#[no_mangle]
pub extern "C" fn blkio_free_mem_region(_b: &mut Blkio, region: &MemRegion) {
    unsafe {
        munmap(region.addr, region.len);
        close(region.fd);
    }
}

#[no_mangle]
pub extern "C" fn blkio_add_mem_region(
    b: &mut Blkio,
    region: &MemRegion,
    errmsg: *mut *mut c_char,
) -> c_int {
    let result = b.add_mem_region(
        region.addr as usize,
        region.len,
        region.fd,
        region.fd_offset,
    );

    set_errmsg_from_result(errmsg, result)
}

#[no_mangle]
pub extern "C" fn blkio_del_mem_region(
    b: &mut Blkio,
    region: &MemRegion,
    errmsg: *mut *mut c_char,
) -> c_int {
    let result = b.del_mem_region(region.addr as usize, region.len);
    set_errmsg_from_result(errmsg, result)
}

#[no_mangle]
pub extern "C" fn blkio_get_queue(b: &mut Blkio, index: c_int) -> *mut Blkioq {
    if index < 0 {
        return ptr::null_mut();
    }

    b.get_queue(index as usize)
        .map(|q| q as *mut Blkioq)
        .unwrap_or(ptr::null_mut())
}

#[no_mangle]
pub extern "C" fn blkioq_read(
    q: &mut Blkioq,
    start: u64,
    buf: *mut c_void,
    len: size_t,
    user_data: *mut c_void,
    flags: u32,
) -> c_int {
    match q.read(start, buf as *mut u8, len, user_data as usize, flags) {
        Ok(_) => 0,
        Err(Error(r, _)) => r,
    }
}

#[no_mangle]
pub extern "C" fn blkioq_write(
    q: &mut Blkioq,
    start: u64,
    buf: *const c_void,
    len: size_t,
    user_data: *mut c_void,
    flags: u32,
) -> c_int {
    match q.write(start, buf as *const u8, len, user_data as usize, flags) {
        Ok(_) => 0,
        Err(Error(r, _)) => r,
    }
}

#[no_mangle]
pub extern "C" fn blkioq_readv(
    q: &mut Blkioq,
    start: u64,
    iovec: *const iovec,
    iovcnt: i32,
    user_data: *mut c_void,
    flags: u32,
) -> c_int {
    if iovcnt < 0 {
        return -EINVAL;
    }
    match q.readv(start, iovec, iovcnt as u32, user_data as usize, flags) {
        Ok(_) => 0,
        Err(Error(r, _)) => r,
    }
}

#[no_mangle]
pub extern "C" fn blkioq_writev(
    q: &mut Blkioq,
    start: u64,
    iovec: *const iovec,
    iovcnt: i32,
    user_data: *mut c_void,
    flags: u32,
) -> c_int {
    if iovcnt < 0 {
        return -EINVAL;
    }
    match q.writev(start, iovec, iovcnt as u32, user_data as usize, flags) {
        Ok(_) => 0,
        Err(Error(r, _)) => r,
    }
}

#[no_mangle]
pub extern "C" fn blkioq_flush(q: &mut Blkioq, user_data: *mut c_void, flags: u32) -> c_int {
    match q.flush(user_data as usize, flags) {
        Ok(_) => 0,
        Err(Error(r, _)) => r,
    }
}

fn duration_from_timespec(t: &timespec) -> Result<Duration> {
    // The Linux kernel checks these timespec fields in the same way. Check now since the same
    // preconditions are necessary for converting to Duration.
    if t.tv_sec < 0 {
        return Err(Error(-EINVAL, "tv_sec cannot be negative".to_string()));
    }
    if t.tv_nsec as u64 >= 1000000000 {
        return Err(Error(
            -EINVAL,
            "tv_nsec must be less than one second".to_string(),
        ));
    }

    Ok(Duration::new(t.tv_sec as u64, t.tv_nsec as u32))
}

#[no_mangle]
pub extern "C" fn blkioq_submit_and_wait(
    q: &mut Blkioq,
    min_completions: c_int,
    timeout: *const timespec,
    sig: *const sigset_t,
) -> c_int {
    let duration = match unsafe { timeout.as_ref() } {
        None => None,
        Some(t) => match duration_from_timespec(t) {
            Ok(d) => Some(d),
            Err(Error(r, _)) => return r,
        },
    };
    let sig_ref = unsafe { sig.as_ref() };
    match q.submit_and_wait(min_completions as u32, duration, sig_ref) {
        Ok(_) => 0,
        Err(Error(r, _)) => r,
    }
}

#[repr(C)]
pub struct BlkioCompletion {
    user_data: *mut c_void,
    ret: c_int,
}

#[no_mangle]
pub extern "C" fn blkioq_get_completions(
    q: &mut Blkioq,
    mut completions: *mut BlkioCompletion,
    count: c_int,
) -> c_int {
    if count < 0 {
        return -EINVAL;
    }
    let mut total = 0;
    for c in q.completions().take(count as usize) {
        unsafe {
            (*completions).user_data = c.user_data as *mut c_void;
            (*completions).ret = c.ret as c_int;
            completions = completions.add(1);
        }
        total += 1;
    }
    total
}

#[no_mangle]
pub extern "C" fn blkioq_get_completion_fd(q: &mut Blkioq) -> c_int {
    match q.get_completion_fd() {
        Ok(fd) => fd,
        Err(Error(r, _)) => r,
    }
}
