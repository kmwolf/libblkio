use libc::{iovec, sigset_t, EINVAL};
use std::error;
use std::fmt;
use std::os::unix::io::RawFd;
use std::result;
use std::time::Duration;

// Must be kept in sync with include/blkio.h
pub const BLKIO_REQ_POLLED: u32 = 1 << 0;
pub const BLKIO_REQ_FUA: u32 = 1 << 1;
const BLKIO_REQ_FLAGS: u32 = BLKIO_REQ_POLLED | BLKIO_REQ_FUA;

#[derive(Debug)]
pub struct Error(pub i32, pub String);

impl Error {
    pub fn from_io_error(err: std::io::Error, default: i32) -> Self {
        if let Some(errno) = err.raw_os_error() {
            return Error(-errno, err.to_string());
        } else {
            return Error(default, err.to_string());
        }
    }
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> result::Result<(), fmt::Error> {
        write!(f, "{}", self.1)
    }
}

pub type Result<T> = result::Result<T, Error>;

pub struct Completion {
    pub user_data: usize,
    pub ret: i32,
}

pub trait Queue {
    fn get_completion_fd(&self) -> Result<i32>;

    fn read(
        &mut self,
        start: u64,
        buf: *mut u8,
        len: usize,
        user_data: usize,
        flags: u32,
    ) -> Result<()>;

    fn write(
        &mut self,
        start: u64,
        buf: *const u8,
        len: usize,
        user_data: usize,
        flags: u32,
    ) -> Result<()>;

    fn readv(
        &mut self,
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
        user_data: usize,
        flags: u32,
    ) -> Result<()>;

    fn writev(
        &mut self,
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
        user_data: usize,
        flags: u32,
    ) -> Result<()>;

    fn flush(&mut self, user_data: usize, flags: u32) -> Result<()>;

    fn submit_and_wait(
        &mut self,
        min_completions: u32,
        timeout: Option<Duration>,
        sig: Option<&sigset_t>,
    ) -> Result<()>;

    fn completions(&mut self) -> Box<dyn Iterator<Item = Completion> + '_>;
}

fn req_flags_must_be_valid(flags: u32) -> Result<()> {
    if flags & !BLKIO_REQ_FLAGS == 0 {
        Ok(())
    } else {
        Err(Error(-EINVAL, "unsupported request flags".to_string()))
    }
}

fn req_flags_cannot_have_fua(flags: u32) -> Result<()> {
    if flags & BLKIO_REQ_FUA == 0 {
        Ok(())
    } else {
        Err(Error(
            -EINVAL,
            "BLKIO_REQ_FUA is invalid for this request type".to_string(),
        ))
    }
}

pub struct Blkioq {
    queue: Box<dyn Queue>,
}

impl Blkioq {
    pub fn new(queue: Box<dyn Queue>) -> Self {
        Blkioq { queue }
    }

    pub fn get_completion_fd(&self) -> Result<i32> {
        self.queue.get_completion_fd()
    }

    pub fn read(
        &mut self,
        start: u64,
        buf: *mut u8,
        len: usize,
        user_data: usize,
        flags: u32,
    ) -> Result<()> {
        req_flags_must_be_valid(flags)?;
        req_flags_cannot_have_fua(flags)?;
        self.queue.read(start, buf, len, user_data, flags)
    }

    pub fn write(
        &mut self,
        start: u64,
        buf: *const u8,
        len: usize,
        user_data: usize,
        flags: u32,
    ) -> Result<()> {
        req_flags_must_be_valid(flags)?;
        self.queue.write(start, buf, len, user_data, flags)
    }

    pub fn readv(
        &mut self,
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
        user_data: usize,
        flags: u32,
    ) -> Result<()> {
        req_flags_must_be_valid(flags)?;
        req_flags_cannot_have_fua(flags)?;
        self.queue.readv(start, iovec, iovcnt, user_data, flags)
    }

    pub fn writev(
        &mut self,
        start: u64,
        iovec: *const iovec,
        iovcnt: u32,
        user_data: usize,
        flags: u32,
    ) -> Result<()> {
        req_flags_must_be_valid(flags)?;
        self.queue.writev(start, iovec, iovcnt, user_data, flags)
    }

    pub fn flush(&mut self, user_data: usize, flags: u32) -> Result<()> {
        req_flags_must_be_valid(flags)?;
        req_flags_cannot_have_fua(flags)?;
        self.queue.flush(user_data, flags)
    }

    pub fn submit_and_wait(
        &mut self,
        min_completions: u32,
        timeout: Option<Duration>,
        sig: Option<&sigset_t>,
    ) -> Result<()> {
        self.queue.submit_and_wait(min_completions, timeout, sig)
    }

    pub fn completions(&mut self) -> Box<dyn Iterator<Item = Completion> + '_> {
        self.queue.completions()
    }
}

pub trait Driver {
    fn get_bool(&self, name: &str) -> Result<bool>;
    fn get_i32(&self, name: &str) -> Result<i32>;
    fn get_str(&self, name: &str) -> Result<String>;
    fn get_u64(&self, name: &str) -> Result<u64>;
    fn set_bool(&mut self, name: &str, value: bool) -> Result<()>;
    fn set_i32(&mut self, name: &str, value: i32) -> Result<()>;
    fn set_str(&mut self, name: &str, value: &str) -> Result<()>;
    fn set_u64(&mut self, name: &str, value: u64) -> Result<()>;
    fn add_mem_region(&mut self, addr: usize, len: usize, fd: RawFd, fd_offset: i64) -> Result<()>;
    fn del_mem_region(&mut self, addr: usize, len: usize) -> Result<()>;
    fn get_queue(&mut self, index: usize) -> Result<&mut Blkioq>;
}

pub struct Blkio {
    driver: Box<dyn Driver>,
}

impl Blkio {
    pub fn new(driver: Box<dyn Driver>) -> Self {
        Blkio { driver }
    }

    pub fn get_bool(&self, name: &str) -> Result<bool> {
        self.driver.get_bool(name)
    }

    pub fn get_i32(&self, name: &str) -> Result<i32> {
        self.driver.get_i32(name)
    }

    pub fn get_str(&self, name: &str) -> Result<String> {
        self.driver.get_str(name)
    }

    pub fn get_u64(&self, name: &str) -> Result<u64> {
        self.driver.get_u64(name)
    }

    pub fn set_bool(&mut self, name: &str, value: bool) -> Result<()> {
        self.driver.set_bool(name, value)
    }

    pub fn set_i32(&mut self, name: &str, value: i32) -> Result<()> {
        self.driver.set_i32(name, value)
    }

    pub fn set_str(&mut self, name: &str, value: &str) -> Result<()> {
        self.driver.set_str(name, value)
    }

    pub fn set_u64(&mut self, name: &str, value: u64) -> Result<()> {
        self.driver.set_u64(name, value)
    }

    pub fn add_mem_region(
        &mut self,
        addr: usize,
        len: usize,
        fd: RawFd,
        fd_offset: i64,
    ) -> Result<()> {
        self.driver.add_mem_region(addr, len, fd, fd_offset)
    }

    pub fn del_mem_region(&mut self, addr: usize, len: usize) -> Result<()> {
        self.driver.del_mem_region(addr, len)
    }

    pub fn get_queue(&mut self, index: usize) -> Result<&mut Blkioq> {
        self.driver.get_queue(index)
    }
}
