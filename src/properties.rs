use crate::blkio::{Error, Result};
use libc::{EACCES, EBUSY, EINVAL, ENODEV, ENOENT, ENOTTY};

/*
 * Drivers are configured via properties through the blkio_get_X()/blkio_set_X() APIs where X is a
 * data type.
 */

pub fn error_read_only() -> Error {
    Error(-EACCES, "Property is read-only".to_string())
}

pub fn error_must_be_initialized() -> Error {
    Error(-ENODEV, "Device must be initialized".to_string())
}

pub fn error_cant_set_while_initialized() -> Error {
    Error(
        -EBUSY,
        "Cannot set property in initialized state".to_string(),
    )
}

pub fn error_cant_set_while_started() -> Error {
    Error(-EBUSY, "Cannot set property in started state".to_string())
}

enum PropertyAccessor<T> {
    Bool(fn(&T) -> Result<bool>, fn(&mut T, bool) -> Result<()>),
    I32(fn(&T) -> Result<i32>, fn(&mut T, i32) -> Result<()>),
    Str(fn(&T) -> Result<String>, fn(&mut T, &str) -> Result<()>),
    U64(fn(&T) -> Result<u64>, fn(&mut T, u64) -> Result<()>),
}

pub struct Property<T> {
    name: String,
    accessor: PropertyAccessor<T>,
}

impl<T> Property<T> {
    pub fn set_error_read_only<U>(_: &mut T, _: U) -> Result<()> {
        Err(error_read_only())
    }

    pub fn new_bool(
        name: &str,
        get: fn(&T) -> Result<bool>,
        set: fn(&mut T, bool) -> Result<()>,
    ) -> Self {
        Property {
            name: String::from(name),
            accessor: PropertyAccessor::Bool(get, set),
        }
    }

    pub fn new_i32(
        name: &str,
        get: fn(&T) -> Result<i32>,
        set: fn(&mut T, i32) -> Result<()>,
    ) -> Self {
        Property {
            name: String::from(name),
            accessor: PropertyAccessor::I32(get, set),
        }
    }

    pub fn new_str(
        name: &str,
        get: fn(&T) -> Result<String>,
        set: fn(&mut T, &str) -> Result<()>,
    ) -> Self {
        Property {
            name: String::from(name),
            accessor: PropertyAccessor::Str(get, set),
        }
    }

    pub fn new_u64(
        name: &str,
        get: fn(&T) -> Result<u64>,
        set: fn(&mut T, u64) -> Result<()>,
    ) -> Self {
        Property {
            name: String::from(name),
            accessor: PropertyAccessor::U64(get, set),
        }
    }

    fn find<'a>(props: &'a [Property<T>], name: &str) -> Result<&'a PropertyAccessor<T>> {
        for prop in props {
            if name == prop.name {
                return Ok(&prop.accessor);
            }
        }
        Err(Error(-ENOENT, "Unknown property name".to_string()))
    }

    pub fn get_bool(props: &[Property<T>], name: &str, t: &T) -> Result<bool> {
        if let PropertyAccessor::Bool(get, _) = Property::find(props, name)? {
            get(t)
        } else {
            Err(Error(-ENOTTY, "Property is not a bool".to_string()))
        }
    }

    pub fn get_i32(props: &[Property<T>], name: &str, t: &T) -> Result<i32> {
        if let PropertyAccessor::I32(get, _) = Property::find(props, name)? {
            get(t)
        } else {
            Err(Error(-ENOTTY, "Property is not an int".to_string()))
        }
    }

    pub fn get_str(props: &[Property<T>], name: &str, t: &T) -> Result<String> {
        match Property::find(props, name)? {
            PropertyAccessor::Bool(get, _) => Ok(get(t)?.to_string()),
            PropertyAccessor::I32(get, _) => Ok(get(t)?.to_string()),
            PropertyAccessor::Str(get, _) => get(t),
            PropertyAccessor::U64(get, _) => Ok(get(t)?.to_string()),
        }
    }

    pub fn get_u64(props: &[Property<T>], name: &str, t: &T) -> Result<u64> {
        if let PropertyAccessor::U64(get, _) = Property::find(props, name)? {
            get(t)
        } else {
            Err(Error(
                -ENOTTY,
                "Property is not an unsigned 64-bit integer".to_string(),
            ))
        }
    }

    pub fn set_bool(props: &[Property<T>], name: &str, t: &mut T, value: bool) -> Result<()> {
        if let PropertyAccessor::Bool(_, set) = Property::find(props, name)? {
            set(t, value)
        } else {
            Err(Error(-ENOTTY, "Property is not a bool".to_string()))
        }
    }

    pub fn set_i32(props: &[Property<T>], name: &str, t: &mut T, value: i32) -> Result<()> {
        if let PropertyAccessor::I32(_, set) = Property::find(props, name)? {
            set(t, value)
        } else {
            Err(Error(-ENOTTY, "Property is not an int".to_string()))
        }
    }

    pub fn set_str(props: &[Property<T>], name: &str, t: &mut T, value: &str) -> Result<()> {
        match Property::find(props, name)? {
            PropertyAccessor::Bool(_, set) => {
                if let Ok(bool_value) = value.parse::<bool>() {
                    set(t, bool_value)
                } else {
                    Err(Error(
                        -EINVAL,
                        "Value must be \"true\" or \"false\"".to_string(),
                    ))
                }
            }
            PropertyAccessor::I32(_, set) => {
                if let Ok(i32_value) = value.parse::<i32>() {
                    set(t, i32_value)
                } else {
                    Err(Error(
                        -EINVAL,
                        "Value must be a signed 32-bit integer".to_string(),
                    ))
                }
            }
            PropertyAccessor::Str(_, set) => set(t, value),
            PropertyAccessor::U64(_, set) => {
                if let Ok(u64_value) = value.parse::<u64>() {
                    set(t, u64_value)
                } else {
                    Err(Error(
                        -EINVAL,
                        "Value must be an unsigned 64-bit integer".to_string(),
                    ))
                }
            }
        }
    }

    pub fn set_u64(props: &[Property<T>], name: &str, t: &mut T, value: u64) -> Result<()> {
        if let PropertyAccessor::U64(_, set) = Property::find(props, name)? {
            set(t, value)
        } else {
            Err(Error(
                -ENOTTY,
                "Property is not an unsigned 64-bit int".to_string(),
            ))
        }
    }
}
